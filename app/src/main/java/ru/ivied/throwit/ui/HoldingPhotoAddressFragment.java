package ru.ivied.throwit.ui;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;

public class HoldingPhotoAddressFragment extends Fragment {

    protected Uri mCurrentPhotoUri;

    private static final String PHOTO_URI = "PhotoUri";

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null && savedInstanceState.getString(PHOTO_URI) != null) {
            mCurrentPhotoUri = Uri.parse(savedInstanceState.getString(PHOTO_URI));
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCurrentPhotoUri != null) outState.putString(PHOTO_URI, mCurrentPhotoUri.toString());
    }
}
