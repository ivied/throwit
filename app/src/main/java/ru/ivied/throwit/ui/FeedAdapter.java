package ru.ivied.throwit.ui;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ru.ivied.throwit.R;
import ru.ivied.throwit.data.entity.Meal;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.PhotoViewHolder>{

    private List<Meal> photos;
    private Context context;


    public FeedAdapter(Context context, List<Meal> photos) {
        this.context = context;
        this.photos = photos;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater. from(viewGroup.getContext()).inflate(R.layout.view_photo, viewGroup, false);

        return new PhotoViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(PhotoViewHolder photoViewHolder, int position) {
        final Meal meal = photos.get(position);
        photoViewHolder.description.setText(new SimpleDateFormat().format(meal.getTimeTaken()));
        Picasso.with(context).load(meal.getLocalUrl()).fit().centerCrop().into(photoViewHolder.photo);
        View.OnClickListener photoClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(meal.link));
                //context.startActivity(browserIntent);
            }
        };
        photoViewHolder.description.setOnClickListener(photoClickListener);
        photoViewHolder.photo.setOnClickListener(photoClickListener);
        photoViewHolder.card.setOnClickListener(photoClickListener);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }


    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.photoView)
        ImageView photo;
        @InjectView(R.id.descriptionTextView)
        TextView description;
        @InjectView(R.id.cardView)
        CardView card;

        public PhotoViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
