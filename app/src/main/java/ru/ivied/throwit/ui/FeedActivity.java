package ru.ivied.throwit.ui;

import android.app.Activity;
import android.os.Bundle;

import ru.ivied.throwit.R;


public class FeedActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        if (savedInstanceState == null ) {

            getFragmentManager().beginTransaction().replace(R.id.fragment_holder, new FeedFragment()).commit();
        }
    }

}
