package ru.ivied.throwit.ui;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;
import com.soundcloud.android.crop.Crop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.animators.FadeInDownAnimator;
import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import ru.ivied.throwit.R;
import ru.ivied.throwit.ThrowItApplication;
import ru.ivied.throwit.bl.MealBusinessLogic;
import ru.ivied.throwit.data.ThrowItContentProvider;
import ru.ivied.throwit.data.entity.Meal;
import ru.ivied.throwit.di.component.AppComponent;
import ru.ivied.throwit.di.component.DaggerFragmentComponent;
import ru.ivied.throwit.di.module.FragmentModule;
import ru.ivied.throwit.network.ApiService;
import ru.ivied.throwit.util.Loaders;
import ru.ivied.throwit.util.Utils;


public class FeedFragment extends HoldingPhotoAddressFragment implements LoaderManager.LoaderCallbacks<List<Meal>> {


    private static final String TAG = FeedFragment.class.getSimpleName();
    private static final int CAMERA_IMAGE_CAPTURE = 0;
    @InjectView(R.id.cardList) RecyclerView cardList;
  //  @InjectView(R.id.swipe_refresh_layout) SwipeRefreshLayout refreshLayout;
    @InjectView(R.id.bAddMeal) FloatingActionButton bAddMeal;


    @Inject  ApiService apiService;
    @Inject ThrowItApplication application;
    @Inject MealBusinessLogic mealBusinessLogic;

    private ScaleInAnimationAdapter feedAdapter;
    private List<Meal> meals = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.inject(this, view);

        getLoaderManager().initLoader(Loaders.SEARCH_ACTIVITY_MATCH_LIST, null, this);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        cardList.setLayoutManager(llm);

        feedAdapter = new ScaleInAnimationAdapter(new AlphaInAnimationAdapter(new FeedAdapter(getActivity(), meals)));
        feedAdapter.setDuration(700);
        cardList.setAdapter(feedAdapter);
       /* refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(false);

            }
        });*/
        DaggerFragmentComponent.builder().fragmentModule(new FragmentModule()).appComponent(getApplicationComponent()).build().inject(this);


        cardList.setItemAnimator(new FadeInDownAnimator());
        return view;
    }



    @Override
    public void onStart() {
        super.onStart();
        apiService.getFeed(callback);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @OnClick(R.id.bAddMeal)
    void onAddMeal(){
        try {
            mCurrentPhotoUri = Utils.createImageFile();
            if (!getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
                return;
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            i.putExtra(MediaStore.EXTRA_OUTPUT, mCurrentPhotoUri);

           startActivityForResult(i, CAMERA_IMAGE_CAPTURE);
        } catch (IOException e){
            Log.e(TAG, "cant create image file for photo", e);
        }

    }


    private Callback<retrofit.client.Response> callback = new Callback<retrofit.client.Response>() {
        @Override
        public void success(retrofit.client.Response photoList, retrofit.client.Response response) {
        }

        @Override
        public void failure(RetrofitError error) {
        }
    };


    protected AppComponent getApplicationComponent() {
        return ((ThrowItApplication)getActivity().getApplication()).getApplicationComponent();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Crop.REQUEST_CROP:
                    mealBusinessLogic.addMeal(mCurrentPhotoUri.toString());
                    break;
                case CAMERA_IMAGE_CAPTURE:
                    beginCrop();
                    break;

            }
        }
    }

    private void beginCrop() {
        Crop.of(mCurrentPhotoUri, mCurrentPhotoUri).start(getActivity(), this);
    }

    @Override
    public Loader<List<Meal>> onCreateLoader(int id, Bundle args) {

        return new AsyncTaskLoader<List<Meal>>(getActivity()) {

            @Override
            protected void onStartLoading() {
                Log.i("Loader", "onStartLoading");
                ThrowItApplication.getInstance().getContentResolver().registerContentObserver(
                        ThrowItContentProvider.MEAL_URI, true, new ForceLoadContentObserver());
                forceLoad();
            }

            @Override
            public List<Meal> loadInBackground() {
                Log.i("Loader", "loadInBackground");
                return mealBusinessLogic.getMeals();
            }
        };
    }

    @Override public void onLoadFinished(Loader<List<Meal>> loader, List<Meal> newMeals) {
        MealBusinessLogic.removeOldMeals(newMeals, meals);
        meals.addAll(newMeals);
        MealBusinessLogic.sortByDateDesc(meals);
        feedAdapter.notifyDataSetChanged();
    }

    @Override public void onLoaderReset(Loader<List<Meal>> loader) {

    }


}
