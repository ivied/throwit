package ru.ivied.throwit.ui;

import android.graphics.Picture;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import ru.ivied.throwit.R;

public class EditMealFragment extends HoldingPhotoAddressFragment {

    private static final String PHOTO = "Photo";
    private static final String PHOTO_URI = "PhotoUri";
    @InjectView(R.id.ivPhoto) ImageView photo;
    private Picture picture;


    public static EditMealFragment newInstance(Picture picture) {

        Bundle data = new Bundle();
        //data.putSerializable(EditMealFragment.PHOTO, picture);
        EditMealFragment photoDialog = new EditMealFragment();
        photoDialog.setArguments(data);
        return photoDialog;
    }

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_meal, container, false);
        ButterKnife.inject(this, view);
        if (getArguments() != null) {
            picture = (Picture) getArguments().getSerializable(PHOTO);
        }
        Picasso.with(getActivity()).load(mCurrentPhotoUri).placeholder(R.drawable.ic_camera_black_48dp).error(android.R.drawable.stat_notify_error).into(photo);
        return view;
    }

    @OnClick(R.id.bAdd) void onAddMeal(){

    }
}
