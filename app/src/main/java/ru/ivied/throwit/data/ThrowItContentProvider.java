package ru.ivied.throwit.data;

import android.net.Uri;

import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
import com.tojc.ormlite.android.framework.MatcherController;
import com.tojc.ormlite.android.framework.MimeTypeVnd;

import ru.ivied.throwit.data.entity.Meal;
import ru.ivied.throwit.data.entity.MealContract;

/**
 * Created by serv on 02/01/15.
 */
public class ThrowItContentProvider extends OrmLiteSimpleContentProvider<DatabaseHelper> {


    public static final Uri MEAL_URI = Uri.parse("content://com.getpure.pure/meal");

    @Override
    protected Class<DatabaseHelper> getHelperClass()
    {
        return DatabaseHelper.class;
    }



    @Override
    public boolean onCreate()
    {
        setMatcherController(new MatcherController()
                        .add(Meal.class, MimeTypeVnd.SubType.DIRECTORY, "", MealContract.CONTENT_URI_PATTERN_MANY)
        );
        return true;
    }
}
