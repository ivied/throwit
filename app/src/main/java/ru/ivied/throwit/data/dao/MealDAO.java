package ru.ivied.throwit.data.dao;


import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Singleton;

import ru.ivied.throwit.data.entity.Meal;

/**
 * Created by serv on 02/01/15.
 */
@Singleton
public class MealDAO extends BaseDaoImpl<Meal, Integer> {

    public MealDAO(ConnectionSource connectionSource, Class<Meal> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Meal> getAllMeal() throws SQLException {
        return this.queryForAll();
    }
}