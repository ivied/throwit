package ru.ivied.throwit.data.entity;

import android.provider.BaseColumns;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

/**
 * Created by serv on 02/01/15.
 */
@AdditionalAnnotation.Contract
@AdditionalAnnotation.DefaultContentUri(authority = "ru.ivied.throwit", path = "meal")
@AdditionalAnnotation.DefaultContentMimeTypeVnd(name = "ru.ivied.throwit.provider" , type = "meal")
@DatabaseTable
public class Meal {

    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)  int id;
    @DatabaseField  String title;
    @DatabaseField  double kcal;
    @DatabaseField  String localUrl;
    @DatabaseField  long timeStamp;
    @DatabaseField  long timeTaken;
    @DatabaseField  String externalUrl;
    @DatabaseField  boolean isRated;


    public Meal() {
    }

    public Meal(String title, double kcal) {
        this.title = title;
        this.kcal = kcal;
    }

    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getKcal() {
        return kcal;
    }

    public void setKcal(double kcal) {
        this.kcal = kcal;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public boolean isRated() {
        return isRated;
    }

    public void setIsRated(boolean isRated) {
        this.isRated = isRated;
    }
}
