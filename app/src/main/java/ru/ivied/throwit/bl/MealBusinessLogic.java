package ru.ivied.throwit.bl;

import android.content.Context;
import android.util.Log;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import ru.ivied.throwit.data.DatabaseHelper;
import ru.ivied.throwit.data.entity.Meal;

public class MealBusinessLogic {

    private static final String TAG = MealBusinessLogic.class.getSimpleName();
    private final DatabaseHelper helper;
    private Context context;

    @Inject MealBusinessLogic(Context context, DatabaseHelper helper){
        this.context = context;

        this.helper = helper;
    }

    public List<Meal> getMeals() {
        List<Meal> matchList = new ArrayList<Meal>();
        try {
            matchList = helper.getMealDao().queryForAll();
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
        return matchList;
    }



    public  void addMeal(String localUrl) {
        Meal meal = new Meal();
        meal.setLocalUrl(localUrl);
        long time = System.currentTimeMillis();
        meal.setTimeStamp(time);
        meal.setTimeTaken(time);
        try {
            helper.getMealDao().create(meal);
        } catch (SQLException e) {
            Log.e(TAG, "Problem with db", e);
        }
    }


    public static void removeOldMeals(final List<Meal> newMeals, final List<Meal> oldMeals) {
        Iterables.removeIf(newMeals, new Predicate<Meal>() {
            @Override public boolean apply(final Meal newMeal) {
                return Iterables.any(oldMeals, new Predicate<Meal>() {
                    @Override public boolean apply(Meal oldMeal) {
                        return oldMeal.getId() == newMeal.getId();
                    }
                });
            }
        });
    }

    public static void sortByDateDesc(List<Meal> meals) {
        Collections.sort(meals, new Comparator<Meal>() {

            @Override public int compare(Meal left, Meal right) {
                return Long.valueOf(right.getTimeTaken()).compareTo(left.getTimeTaken());
            }
        });
    }
}
