package ru.ivied.throwit.di.component;

import dagger.Component;
import ru.ivied.throwit.di.PerActivity;
import ru.ivied.throwit.di.module.ActivityModule;
import ru.ivied.throwit.ui.FeedActivity;

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(FeedActivity activity);
}
