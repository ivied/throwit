package ru.ivied.throwit.di.module;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.ivied.throwit.ThrowItApplication;

@Module
public class AppModule {
    private final ThrowItApplication app;

    public AppModule(ThrowItApplication application) {
        app = application;
    }

    @Provides
    @Singleton
    protected ThrowItApplication provideApplication() {
        return app;
    }


    @Provides
    Context getContext(){
        return app;

    }

}
