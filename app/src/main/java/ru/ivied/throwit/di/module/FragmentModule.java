package ru.ivied.throwit.di.module;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import ru.ivied.throwit.di.PerActivity;
import ru.ivied.throwit.network.ApiService;

@Module
public class FragmentModule {


    public static final String ENDPOINT = "https://www.flickr.com";

    @Provides
    @PerActivity ApiService provideFlickrService() {
        return new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .build()
                .create(ApiService.class);
    }
}
