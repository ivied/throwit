package ru.ivied.throwit.di.component;

import dagger.Component;
import ru.ivied.throwit.di.PerActivity;
import ru.ivied.throwit.di.module.FragmentModule;
import ru.ivied.throwit.ui.FeedFragment;

@Component(dependencies = AppComponent.class, modules = FragmentModule.class)
@PerActivity
public interface FragmentComponent {
    //Exposed to sub-graphs.

    void inject(FeedFragment fragment);
}