package ru.ivied.throwit.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.ivied.throwit.ThrowItApplication;
import ru.ivied.throwit.di.module.AppModule;

@Component(
        modules = {
                AppModule.class
        }
)
@Singleton
public interface AppComponent {
        ThrowItApplication app();
        Context context();

}
