package ru.ivied.throwit.util;

import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class Utils {

    public static Uri createImageFile() throws IOException{
        String imageFileName = "JPEG_" + new Date().getTime();
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = getPhotoPath(imageFileName, storageDir);
        return Uri.fromFile(image);

    }


    private static File getPhotoPath(String imageFileName, File storageDir) throws IOException {
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }

}
