package ru.ivied.throwit;

import android.app.Application;

import ru.ivied.throwit.di.component.AppComponent;
import ru.ivied.throwit.di.component.DaggerAppComponent;
import ru.ivied.throwit.di.module.AppModule;

/**
 * Created by serv on 02/01/15.
 */
public class ThrowItApplication extends Application {


    private static ThrowItApplication instance;
    private AppComponent graph;


    public static ThrowItApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        initializeInjector();
    }

    private void initializeInjector() {
        graph = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

    public AppComponent getApplicationComponent() {
        return graph;
    }
}
