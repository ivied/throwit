package ru.ivied.throwit;

import android.content.ContentValues;
import android.database.Cursor;
import android.test.ProviderTestCase2;

import java.sql.SQLException;

import ru.ivied.throwit.data.DatabaseHelper;
import ru.ivied.throwit.data.ThrowItContentProvider;
import ru.ivied.throwit.data.dao.MealDAO;
import ru.ivied.throwit.data.entity.Meal;
import ru.ivied.throwit.data.entity.MealContract;

/**
 * Created by serv on 02/01/15.
 */
public class ProviderTest extends ProviderTestCase2 {

    public ProviderTest() {
        super(ThrowItContentProvider.class, MealContract.AUTHORITY);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    public void testWriteReadToDataBase() throws SQLException{

       // DatabaseHelper.getDatabaseHelper(getContext()).getMealDao().
       // DatabaseHelper.getDatabaseHelper(getContext()).getMealDao().create(new Meal("Rice", 500));

        ContentValues meal = new ContentValues();
        meal.put(MealContract.KCAL, 500);
        meal.put(MealContract.TITLE, "Rice");
        getContext().getContentResolver().insert(MealContract.CONTENT_URI, meal);
        Cursor cursor = getMockContentResolver().query(MealContract.CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        String title = cursor.getString(2);
        assertEquals("Rice", title);
    }
}
